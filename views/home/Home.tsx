import classNames from 'classnames'

import { Layout } from '~/components'
import { Card } from './Card'

import { cards } from './cards'

import styles from './Home.module.scss'

const Home = () => {
    return (
        <Layout>
            <div className={ styles.hero }>
                <div className="container d-flex justify-content-between align-items-center">
                    <div>
                        <div className={ styles.mottoContainer }>
                            <h3 className={ styles.motto }>Помогаем школьникам обрести профессию</h3>
                            <h4 className={ classNames('mt-3', styles.mottoSubtitle) }>Задаём направление</h4>
                        </div>

                        <button className={ classNames('btn btn-primary btn-lg mt-5', styles.loginButton) }>
                            Личный кабинет
                        </button>
                    </div>
                    <img className={ styles.heroImage } src="/images/hero-image.png" alt="" />
                </div>
            </div>

            <div className={ classNames('container', styles.cards) }>
                <div className="row">
                    {
                        cards.map(card => (
                            <div className="col py-3">
                                <Card
                                    items={ card.items }
                                    title={ card.title }
                                    url={ card.url }
                                />
                            </div>
                        ))
                    }
                </div>
            </div>
        </Layout>
    )
}

export { Home }
