import { FC } from 'react'

interface CardProps {
    items: string[]
    title: string
    url: string
}

const Card: FC<CardProps> = ({
    items,
    title,
    url,
}) => {
    return (
        <div className="d-flex flex-column align-items-center h-100 p-4 bg-white rounded-3 shadow">
            <h4>{ title }</h4>
            <ul className="mt-3 mb-4 list-unstyled text-center">
                {
                    items.map((item, index) => (
                        <li
                            key={ index }
                            className="mb-2"
                        >{ item }</li>
                    ))
                }
            </ul>
            <a href={ url } type="button" className="btn btn-primary btn-lg mt-auto">
                Подробнее
            </a>
        </div>
    )
}

export { Card }
