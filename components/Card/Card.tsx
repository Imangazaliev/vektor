import { FC } from 'react'
import classNames from 'classnames'

interface CardProps {
    className?: string
}

const Card: FC<CardProps> = ({
    children,
    className,
}) => {
    return (
        <div className={ classNames('d-flex flex-column h-100 p-4 bg-white rounded-3 shadow-sm', className) }>
            { children }
        </div>
    )
}

export { Card }
