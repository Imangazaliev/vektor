import styles from './Footer.module.scss'

const Footer = () => {
    return (
        <footer className={ styles.root }>
            <div className="container">
                <div className="row">
                    <div className="col col-3 px-4">
                        <h5 className="text-light">Компания</h5>
                        <ul className={ styles.list }>
                            <li>
                                <a href="#">О нас</a>
                            </li>
                            <li>
                                <a href="#">Контакты</a>
                            </li>
                            <li>
                                <a href="#">Вакансии</a>
                            </li>
                            <li>
                                <a href="#">Блог</a>
                            </li>
                        </ul>
                    </div>
                    <div className="col col-3 px-4">
                        <h5 className="text-light">Обучение</h5>
                        <ul className={ styles.list }>
                            <li>
                                <a href="#">Курсы</a>
                            </li>
                            <li>
                                <a href="#">Поступление</a>
                            </li>
                            <li>
                                <a href="#">Стажировки</a>
                            </li>
                            <li>
                                <a href="#">Рейтинг</a>
                            </li>
                        </ul>
                    </div>
                    <div className="col col-2 px-4">
                    </div>
                    <div className="col col-4 px-4">
                        <h5 className="text-light">Контакты</h5>

                        <div>
                            <a className="text-light fs-4" href="tel:+78005554405">
                                8 800 555 44 05
                            </a>
                        </div>

                        <div>
                            <a className="text-light fs-4" href="tel:+78005554405">
                                info@vektor-school.com
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export { Footer }
